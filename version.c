/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2009 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include <stdio.h>
#include <sys/time.h>
#include <libsmbclient.h>
#include <fuse.h>
#include "version.h"


#if ((USMB_VERSION_STATUS != 'a') && \
     (USMB_VERSION_STATUS != 'b') && \
     (USMB_VERSION_STATUS != 'p') && \
     (USMB_VERSION_STATUS != 's'))
  #error Unsupported USMB_VERSION_STATUS
#endif


void show_about (FILE *fp)
{
  fprintf (fp, "usmb - mount SMB shares via FUSE and Samba\n"
               "\n"
               "Copyright (C) 2006-2013 Geoff Johnstone.\n"
               "Licensed under the GNU General Public License.\n"
               "usmb comes with ABSOLUTELY NO WARRANTY; "
                 "for details please see\n"
               "http://www.gnu.org/licenses/gpl.txt\n"
               "\n"
               "Please send bug reports, patches etc. to %s@%s.org\n",
               "geoffjohnstone", "acm");   // anti-spam.
}


static inline const char * get_status (char ch)
{
  switch (ch)
  {
    case 'a': return "alpha";
    case 'b': return "beta";
    case 'p': return "pre-release";
    case 's': return "stable";
    default:  return "unofficial";
  }
}


void show_version (FILE *fp)
{
  show_about (fp);
  fputc ('\n', fp);
  fprintf (fp, "usmb version: %08x (%s)\n"
               "FUSE version: %d.%d\n"
               "Samba version: %s\n",
           USMB_VERSION, get_status (USMB_VERSION_STATUS),
           FUSE_MAJOR_VERSION, FUSE_MINOR_VERSION,
           smbc_version());
}

