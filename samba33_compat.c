/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2009 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "samba3x-compat.h"
#include "usmb.h"
#include "utils.h"


int usmb_statfs (const char *path, struct statvfs *vfs)
{
  if ((NULL == path) || (NULL == vfs))
    return -EINVAL;

  char *url = make_url (path);
  if (NULL == url)
    return -ENOMEM;

  DEBUG (fprintf (stderr, "statfs (%s, %p)\n", url, (void *)vfs));
  memset (vfs, 0, sizeof (*vfs));

  int ret = (0 > smbc_getFunctionStatVFS (ctx) (ctx, url, vfs)) ? -errno : 0;
  free (url);
  return ret;
}


int compat_truncate (const char *path UNUSED, SMBCFILE *file, off_t size)
{
  return (0 > smbc_getFunctionFtruncate (ctx) (ctx, file, size)) ? -errno : 0;
}

