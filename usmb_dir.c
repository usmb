/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2013 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include <sys/time.h>        // struct timeval needed by libsmbclient.h
#include <libsmbclient.h>
#include "samba3x-compat.h"
#include <fuse.h>
#include <dirent.h>
#include <errno.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "usmb_dir.h"
#include "usmb.h"
#include "utils.h"


int usmb_mkdir (const char *dirname, mode_t mode)
{
  char *url = make_url (dirname);
  if (NULL == url)
    return -ENOMEM;

  DEBUG (fprintf (stderr, "mkdir (%s)\n", url));
  int ret = smbc_getFunctionMkdir (ctx) (ctx, url, mode) ? -errno : 0;
  free (url);
  return ret;
}


int usmb_rmdir (const char *dirname)
{
  char *url = make_url (dirname);
  if (NULL == url)
    return -ENOMEM;

  DEBUG (fprintf (stderr, "rmdir (%s)\n", url));
  int ret = smbc_getFunctionRmdir (ctx) (ctx, url) ? -errno : 0;
  free (url);

  return ret;
}


int usmb_opendir (const char *dirname, struct fuse_file_info *fi)
{
  char *url = make_url (dirname);
  if (NULL == url)
    return -ENOMEM;

  DEBUG (fprintf (stderr, "opendir (%s)", url));
  SMBCFILE *file = smbc_getFunctionOpendir (ctx) (ctx, url);
  DEBUG (fprintf (stderr, " = %p\n", (void *)file));

  int ret = (NULL == file) ? -errno : 0;
  free (url);
  fi->fh = smbcfile_to_fd (file);

  return ret;
}


int usmb_readdir (const char *path, void *h, fuse_fill_dir_t filler,
                  off_t offset UNUSED, struct fuse_file_info *fi UNUSED)
{
  SMBCCTX *ctx_ = NULL;
  SMBCFILE *file = NULL;
  char *url = NULL;
  struct smbc_dirent *dirent;
  int ret = 0;

  DEBUG (fprintf (stderr, "readdir (%s)\n", path));

  if (!create_smb_context (&ctx_))
    return -errno;

  do
  {
    url = make_url (path);
    if (NULL == url)
    {
      ret = -ENOMEM;
      break;
    }

    file = smbc_getFunctionOpendir (ctx_) (ctx_, url);
    if (NULL == file)
    {
      ret = -errno;
      break;
    }

    smbc_getFunctionLseekdir (ctx_) (ctx_, file, 0);

    while (NULL != (dirent = smbc_getFunctionReaddir (ctx_) (ctx_, file)))
    {
      struct stat stbuf;

      switch (dirent->smbc_type)
      {
        case SMBC_DIR:
          stbuf.st_mode = DT_DIR << 12;
          break;

        case SMBC_FILE:
          stbuf.st_mode = DT_REG << 12;
          break;

        case SMBC_LINK:
          stbuf.st_mode = DT_LNK << 12;
          break;

        default:
          break;
      }

      DEBUG (fprintf (stderr, "  %s\n", dirent->name));
      if (1 == filler (h, dirent->name, &stbuf, 0))  /* if error */
      {
        ret = -1;
        break;
      }
    }
  } while (false /*CONSTCOND*/);

  if (NULL != file)
    (void)smbc_getFunctionClosedir (ctx_) (ctx_, file);

  if (NULL != url)
    free (url);

  destroy_smb_context (ctx_, 0);
  return ret;
}


int usmb_releasedir (const char *path UNUSED, struct fuse_file_info *fi)
{
  SMBCFILE *file = fd_to_smbcfile (fi->fh);
  DEBUG (fprintf (stderr, "releasedir (%s, %p)\n", path, (void *)file));
  return (0 > smbc_getFunctionClosedir (ctx) (ctx, file)) ? -errno : 0;
}


int usmb_setxattr (const char *path, const char *name, const char *value,
                   size_t size, int flags)
{
  char *url = make_url (path);
  if (NULL == url)
    return -ENOMEM;

  DEBUG (fprintf (stderr, "setxattr (%s, %s, %p, %llu, %x)\n",
                  path, url, value, (unsigned long long)size, flags));
  int ret = smbc_getFunctionSetxattr (ctx) (ctx, url, name,
                                            value, size, flags) ? -errno : 0;
  free (url);
  return ret;
}


int usmb_getxattr (const char *path, const char *name, char *value, size_t size)
{
  char *url = make_url (path);
  if (NULL == url)
    return -ENOMEM;

  DEBUG (fprintf (stderr, "getxattr (%s, %s, %p, %llu)\n",
                  path, url, value, (unsigned long long)size));
  int ret = smbc_getFunctionGetxattr (ctx) (ctx, url, name,
                                            value, size) ?  -errno : 0;
  free (url);
  return ret;
}


int usmb_listxattr (const char *path, char *list, size_t size)
{
  char *url = make_url (path);
  if (NULL == url)
    return -ENOMEM;

  DEBUG (fprintf (stderr, "listxattr (%s, %p, %llu)\n",
                  url, list, (unsigned long long)size));
  int ret = smbc_getFunctionListxattr (ctx) (ctx, url, list, size) ? -errno : 0;
  free (url);
  return ret;
}


int usmb_removexattr (const char *path, const char *name)
{
  char *url = make_url (path);
  if (NULL == url)
    return -ENOMEM;

  DEBUG (fprintf (stderr, "removexattr (%s, %s)\n", url, name));
  int ret = smbc_getFunctionRemovexattr (ctx) (ctx, url, name) ? -errno : 0;
  free (url);
  return ret;
}

