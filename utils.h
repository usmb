/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2009 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_H
  #define UTILS_H

  #include <stdarg.h>
  #include <stdbool.h>
  #include <stddef.h>

  #ifdef DEBUG
    #undef DEBUG
    #define DEBUG(x) (x)
    #define DEBUGON
  #else
    #define DEBUG(x) ((void)0)
  #endif

  char * concat_strings (int num, ...) MUSTCHECK;
  char * xstrdup (const char *in) MUSTCHECK;
  void xfree (const void *ptr);
  void clear_and_free (char *ptr);
  void free_errno (const void *ptr);
  void xfree_errno (const void *ptr);
  bool bsnprintf (char *str, size_t size, const char *format, ...)
    __attribute__ ((format (printf, 3, 4))) MUSTCHECK;
  bool bvsnprintf (char *str, size_t size,
                   const char *format,
                   va_list ap) MUSTCHECK;
  bool baprintf (char **out, const char *format, ...)
    __attribute__ ((format (printf, 2, 3))) MUSTCHECK;
  bool bvaprintf (char **out, const char *format, va_list ap) MUSTCHECK;

#endif
