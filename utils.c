/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2013 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"



#define INIT_LEN 256
char * concat_strings (int num, ...)
{
  va_list ap;

  char *base, *out;

  base = out = malloc (INIT_LEN);
  if (NULL == base)
    return NULL;

  size_t buff_size = INIT_LEN;

  va_start (ap, num);
  for (int i = 0; i < num; ++i)
  {
    const char *next = va_arg (ap, const char *);
    assert (NULL != next);

    size_t next_len = strlen (next);
    /*LINTED*/
    size_t required = (out - base) + next_len + 1;

    if (buff_size < required)
    {
      while (buff_size < required)
      {
        size_t dbl_len = buff_size * 2;
        if (dbl_len < buff_size)
        {
          free (base);
          va_end (ap);
          return NULL;
        }

        buff_size = dbl_len;
      }

      /*LINTED*/
      ptrdiff_t diff = out - base;

      char *newbase = realloc (base, buff_size);
      if (NULL == newbase)
      {
        free (base);
        va_end (ap);
        return NULL;
      }

      base = newbase;
      out = base + diff;
    }

    memcpy (out, next, next_len);
    out += next_len;
  }
  va_end (ap);

  *out = '\0';
  return base;
}


char * xstrdup (const char *in)
{
  char *out = NULL;

  if (NULL != in)
  {
    size_t len = strlen (in) + 1;

    if ((out = malloc (len)))
      memcpy (out, in, len);
  }

  return out;
}


// the const here lets us pass a pointer to const
void xfree (const void *ptr)
{
  if (NULL != ptr)
    free ((void *)ptr);
}


void clear_and_free (char *ptr)
{
  if (NULL != ptr)
  {
    for (char *pch = ptr; '\0' != *pch; ++pch)
      *pch = '\0';

    free (ptr);
  }
}


void free_errno (const void *ptr)
{
  int err = errno;
  free ((void *)ptr);
  errno = err;
}


void xfree_errno (const void *ptr)
{
  int err = errno;
  xfree (ptr);
  errno = err;
}


bool bsnprintf (char *str, size_t size, const char *format, ...)
{
  va_list ap;
  bool ret;

  va_start (ap, format);
  ret = bvsnprintf (str, size, format, ap);
  va_end (ap);

  return ret;
}


bool bvsnprintf (char *str, size_t size, const char *format, va_list ap)
{
  int ret;

  assert (NULL != str);
  assert (NULL != format);

  ret = vsnprintf (str, size, format, ap);

  return ((ret >= 0) && ((size_t)ret < size));
}


bool baprintf (char **out, const char *format, ...)
{
  va_list ap;
  bool ret;

  va_start (ap, format);
  ret = bvaprintf (out, format, ap);
  va_end (ap);

  return ret;
}


bool bvaprintf (char **out, const char *format, va_list ap)
{
  assert (NULL != out);
  assert (NULL != format);
  *out = NULL;

  va_list ap2;
  va_copy (ap2, ap);
  int bytes = vsnprintf (NULL, 0, format, ap2);
  va_end (ap2);

  if ((1 > bytes) || (INT_MAX == bytes))
    return false;

  ++bytes;  // '\0'.

  if (bytes != (int)(size_t)bytes)
    return false;

  *out = malloc ((size_t)bytes);
  if (NULL == *out)
    return false;

  if (!bvsnprintf (*out, bytes, format, ap))
  {
    free (*out);
    *out = NULL;
    return false;
  }

  return true;
}

