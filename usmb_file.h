/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2009 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USMB_FILE_H
  #define USMB_FILE_H

  #include <fuse.h>
  #include <stddef.h>
  #include <sys/types.h>

  int usmb_getattr (const char *filename, struct stat *st);
  int usmb_fgetattr (const char *filename, struct stat *st,
                     struct fuse_file_info *fi);
  int usmb_unlink (const char *filename);
  int usmb_open (const char *filename, struct fuse_file_info *fi);
  int usmb_release (const char *filename, struct fuse_file_info *fi);
  int usmb_read (const char *filename, char *buff, size_t len, off_t off,
                 struct fuse_file_info *fi);
  int usmb_write (const char *filename, const char *buff, size_t len, off_t off,
                  struct fuse_file_info *fi);
  int usmb_create (const char *filename, mode_t mode,
                   struct fuse_file_info *fi);
  int usmb_rename (const char *from, const char *to);
  int usmb_utime (const char *filename, struct utimbuf *utb);
  int usmb_truncate (const char *filename, off_t newsize);
  int usmb_chmod (const char *filename, mode_t mode);
  int usmb_ftruncate (const char *path, off_t size,
                      struct fuse_file_info *fi);

#endif
