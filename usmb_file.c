/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2009 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include <sys/time.h>        // struct timeval needed by libsmbclient.h
#include <libsmbclient.h>
#include "samba3x-compat.h"
#include <limits.h>
#include <assert.h>
#include <fuse.h>
#include <errno.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "usmb_file.h"
#include "usmb.h"
#include "utils.h"


// Samba gets st_nlink wrong for directories.
static bool fix_nlink (const char *url, struct stat *st)
{
  assert (NULL != url);
  assert (NULL != st);

  if (!S_ISDIR (st->st_mode))
    return true;

  SMBCFILE *file = smbc_getFunctionOpendir (ctx) (ctx, url);
  if (NULL == file)
    return false;

  // st->st_nlink = 2;
  st->st_nlink = 0;
  errno = ERANGE;

  struct smbc_dirent *dirent;

  while (NULL != (dirent = smbc_getFunctionReaddir (ctx) (ctx, file)))
    if (SMBC_DIR == dirent->smbc_type)
      if (INT_MAX == st->st_nlink++)
        break;

  (void)smbc_getFunctionClosedir (ctx) (ctx, file);
  return (NULL == dirent);
}


int usmb_getattr (const char *filename, struct stat *st)
{
  char *url = make_url (filename);
  if (NULL == url)
    return -ENOMEM;

  DEBUG (fprintf (stderr, "stat (%s)\n", url));

  int ret = smbc_getFunctionStat (ctx) (ctx, url, st);

  if ((0 > ret) || !fix_nlink (url, st))
    ret = -errno;

  free (url);
  return ret;
}


int usmb_fgetattr (const char *filename UNUSED, struct stat *st,
                   struct fuse_file_info *fi)
{
  SMBCFILE *file = fd_to_smbcfile (fi->fh);
  DEBUG (fprintf (stderr, "fgetattr (%s, %p)\n", filename, (void *)file));

  if (0 > smbc_getFunctionFstat (ctx) (ctx, file, st))
    return -errno;

  if (S_ISDIR (st->st_mode))
  {
    char *url = make_url (filename);
    if (NULL == url)
      return -ENOMEM;

    bool ok = fix_nlink (url, st);
    free_errno (url);

    if (!ok)
      return -errno;
  }

  return 0;
}


int usmb_unlink (const char *filename)
{
  char *url = make_url (filename);
  if (NULL == url)
    return -ENOMEM;

  DEBUG (fprintf (stderr, "unlink (%s)\n", url));
  int ret = (0 > smbc_getFunctionUnlink (ctx) (ctx, url)) ? -errno : 0;
  free (url);
  return ret;
}


int usmb_open (const char *filename, struct fuse_file_info *fi)
{
  char *url = make_url (filename);
  if (NULL == url)
    return -ENOMEM;

  DEBUG (fprintf (stderr, "open (%s, %d)", url, fi->flags));
  SMBCFILE *file = smbc_getFunctionOpen (ctx) (ctx, url, fi->flags, 0);
  DEBUG (fprintf (stderr, " = %p\n", (void *)file));

  int ret = (NULL == file) ? -errno : 0;
  free (url);
  fi->fh = smbcfile_to_fd (file);

  return ret;
}


int usmb_release (const char *filename UNUSED, struct fuse_file_info *fi)
{
  SMBCFILE *file = fd_to_smbcfile (fi->fh);
  DEBUG (fprintf (stderr, "release (%s, %p)\n", filename, (void *)file));
  return (0 > smbc_getFunctionClose (ctx) (ctx, file)) ? -errno : 0;
}


int usmb_read (const char *filename UNUSED, char *buff, size_t len, off_t off,
               struct fuse_file_info *fi)
{
  assert (32768 >= len);

  SMBCFILE *file = fd_to_smbcfile (fi->fh);
  DEBUG (fprintf (stderr, "read (%p, %p, %llu, %lld) ",
                  (void *)file, buff, (unsigned long long)len, (long long)off));

  if (0 > smbc_getFunctionLseek (ctx) (ctx, file, off, SEEK_SET))
  {
    fprintf (stderr, "- seek failed: %d\n", -errno);
    return -errno;
  }

  int bytes = smbc_getFunctionRead (ctx) (ctx, file, buff, len);
  DEBUG (fprintf (stderr, "= %d\n", bytes));
  return (0 > bytes) ? -errno : (int)bytes;
}


int usmb_write (const char *filename UNUSED, const char *buff, size_t len,
                off_t off, struct fuse_file_info *fi)
{
  SMBCFILE *file = fd_to_smbcfile (fi->fh);
  DEBUG (fprintf (stderr, "write (%p, %p, len=%llu, off=%lld) ",
                  (void *)file, buff, (unsigned long long)len, (long long)off));

  if (0 > smbc_getFunctionLseek (ctx) (ctx, file, off, SEEK_SET))
    return -errno;

  size_t written = 0;
  int bytes = 0;

  // No idea whether Windows servers don't like > 32768 byte writes
  // (cf. usmb_read), but taking no chances...

  const smbc_write_fn write_fn = smbc_getFunctionWrite (ctx);

  while (written < len)
  {
    bytes = write_fn (ctx, file, (char *)buff, (len > 32768) ? 32768 : len);
    if (0 > bytes)
      break;

    written += bytes;
    buff += bytes;

    // avoids infinite loops
    if (0 == bytes)
      break;
  }

  DEBUG (fprintf (stderr, "= %d\n", (0 > bytes) ? -errno : (int)written));
  return (0 > bytes) ? -errno : (int)written;
}


int usmb_create (const char *filename, mode_t mode, struct fuse_file_info *fi)
{
  char *url = make_url (filename);
  if (NULL == url)
    return -ENOMEM;

  DEBUG (fprintf (stderr, "creat (%s)", url));

  SMBCFILE *file = smbc_getFunctionCreat (ctx) (ctx, url, mode);
  DEBUG (fprintf (stderr, " = %p\n", (void *)file));
  int ret = (NULL == file) ? -errno : 0;
  fi->fh = smbcfile_to_fd (file);

  free (url);
  return ret;
}


int usmb_rename (const char *from, const char *to)
{
  char *fromurl = make_url (from);
  if (NULL == fromurl)
    return -ENOMEM;

  char *tourl = make_url (to);
  if (NULL == tourl)
  {
    free (fromurl);
    return -ENOMEM;
  }

  DEBUG (fprintf (stderr, "rename (%s, %s)\n", fromurl, tourl));
  int ret =
    (0 > smbc_getFunctionRename (ctx) (ctx, fromurl, ctx, tourl)) ? -errno : 0;
  free (tourl);
  free (fromurl);
  return ret;
}


int usmb_utime (const char *filename, struct utimbuf *utb)
{
  struct utimbuf tmp_utb;

  if (NULL == utb)
  {
    for (;;)
    {
      time_t now = time (NULL);
      if ((time_t)-1 != now)
      {
        tmp_utb.actime = tmp_utb.modtime = now;
        break;
      }

      if (EINTR != errno)
        return -errno;
    }

    utb = &tmp_utb;
  }

  char *url = make_url (filename);
  if (NULL == url)
    return -ENOMEM;

  struct timeval tv[2] = {
    { .tv_sec = utb->actime,  .tv_usec = 0 },
    { .tv_sec = utb->modtime, .tv_usec = 0 },
  };

  DEBUG (fprintf (stderr, "utime (%s)\n", url));
  int ret = (0 > smbc_getFunctionUtimes (ctx) (ctx, url, tv)) ? -errno : 0;
  free (url);
  return ret;
}


#if 0
Samba defines utimes as taking struct timevals rather than timespecs.
int usmb_utimes (const char *filename, const struct timespec ts[2])
{
  char *url = make_url (filename);
  if (NULL == url)
    return -ENOMEM;

  DEBUG (fprintf (stderr, "utimes (%s)\n", url));
  int ret = (0 > ctx->utimes (ctx, url, ts)) ? -errno : 0;
  free (url);
  return ret;
}
#endif


int usmb_chmod (const char *filename, mode_t mode)
{
  char *url = make_url (filename);
  if (NULL == url)
    return -ENOMEM;

  DEBUG (fprintf (stderr, "chmod (%s, %u)\n", url, mode));
  int ret = (0 > smbc_getFunctionChmod (ctx) (ctx, url, mode)) ? -errno : 0;
  free (url);
  return ret;
}


int usmb_truncate (const char *filename, off_t offset)
{
  char *url = make_url (filename);
  if (NULL == url)
    return -ENOMEM;

  SMBCFILE *file = smbc_getFunctionOpen (ctx) (ctx, url, O_WRONLY, 0);
  if (NULL == file)
  {
    int ret = -errno;
    free (url);
    return ret;
  }

  int ret = compat_truncate (filename, file, offset);

  smbc_getFunctionClose (ctx) (ctx, file);
  free (url);
  return ret;
}


int usmb_ftruncate (const char *path, off_t size,
                    struct fuse_file_info *fi)
{
  return compat_truncate (path, fd_to_smbcfile (fi->fh), size);
}

