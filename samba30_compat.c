/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2009 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "samba3x-compat.h"
#include "usmb.h"
#include "usmb_file.h"
#include "utils.h"


int usmb_statfs (const char *path UNUSED, struct statvfs *vfs UNUSED)
{
  (void)path;
  (void)vfs;
  return -ENOSYS;
}


int compat_truncate (const char *path UNUSED, SMBCFILE *file UNUSED, off_t size)
{
  /* Windows doesn't support truncation so we implement a limited version:
   *  0 == size => create a new file for writing.
   *  current size == size => succeed.
   *  else return -ENOSYS.
   */
  
  if (0 == size)
  { 
    char *url = make_url (path);
    if (NULL == url)
      return -ENOMEM;

    SMBCFILE *file_ = smbc_getFunctionOpen (ctx) (ctx, url,
                                                  O_WRONLY | O_TRUNC, 0);
    if (NULL == file_)
    { 
      free_errno (url);
      return -errno;
    }
    
    smbc_getFunctionClose (ctx) (ctx, file_);
    free (url);

    return 0;
  } 

  struct stat st;
  int ret = usmb_getattr (path, &st);

  if (0 != ret)
    return ret;

  return (size == st.st_size) ? 0 : -ENOSYS;
}

